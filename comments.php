<?php
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area"><?php

	if ( have_comments() ) : ?>
		<h2><?php _e("Other people's views", "jigowatt"); ?></h2>

		<ol class="comment-list"><?php
			wp_list_comments( array(
				'style'       => 'ol',
				'short_ping'  => true,
				'avatar_size' => 56,
			) ); ?>
		</ol><!-- .comment-list --><?php

	endif; // have_comments()


	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed, sorry.', 'jigowatt' ); ?></p><?php
	endif;


	// comment form ?>
	<div class="comment-form-container panel"><?php
		$args = array(
			'title_reply' => 'Have your say:',
			'fields' => apply_filters( 'comment_form_default_fields', array(
				'author' =>
					'<div class="grid"><div class="grid__item one-whole lap-and-up-one-half">'.
						'<p class="comment-form-author">' .
							'<label for="author">' . __( 'Your name' ) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .
							'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" />' .
						'</p>'.
					'</div>',

				'email' =>
				'<div class="grid__item one-whole lap-and-up-one-half">'.
					'<p class="comment-form-email">' .
						'<label for="email">' . __( 'Your email' ) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .
						'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" />'.
					'</p>'.
				'</div></div>',

				'url' => ''
			) ),

			'comment_field' =>
				'<p>' .
					'<label for="comment">' . __( 'Your words' ) . '</label>' .
					'<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>' .
				'</p>',

			'comment_notes_after' => '',
		);
		comment_form( $args ); ?>
	</div>

</div><!-- .comments-area -->