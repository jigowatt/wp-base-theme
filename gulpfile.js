/**
 * GULPFILE.JS
 *
 * This file defines how images, styles, scripts and fonts are compiled
 * for our theme and puts them in the correct destination folders.
 *
 */'use strict';


/**
 * Variables
 */
var is_dev = false;
var paths = {
	src : './src/',
	dist: './dist/',
	js : {
		src:  './src/js/',
		dist: './dist/js/'
	},
	img : {
		src:  './src/img/',
		dist: './dist/img/'
	},
	style : {
		src:  './src/scss/',
		dist: './'
	}
}

/**
 * Dependencies
 */
var gulp          = require('gulp');                //gulp
var watch         = require('gulp-watch');          //watch
var runSequence   = require('run-sequence');        //run tasks in order
var clean         = require('gulp-clean');          //deletes files/folders
var sass          = require('gulp-ruby-sass');      //compiles sass
var autoprefixer  = require('gulp-autoprefixer');   //prefixes css properties
var imagemin      = require('gulp-imagemin');       //compresses images
var jshint        = require('gulp-jshint');         //lints JS
var babelify      = require('babelify');            //ES2015 JS transpiler
var browserify    = require('browserify');          //needed for babelify
var source        = require('vinyl-source-stream'); //needed for babelify
var stripDebug    = require('gulp-strip-debug');    //strips console.log etc from JS
var uglify        = require('gulp-uglify');         //compresses JS


/**
 * Housekeeping
 */
gulp.task('housekeeping', function(){
	return gulp.src(paths.dist)
		.pipe(clean());
});


/**
 * Sass compilation
 */
gulp.task('sass', function () {
	var opts = {
		'style': (is_dev) ? 'compact' : 'compressed',
		'stopOnError': true
	};

	return sass(paths.style.src+'style.scss', opts)
		.on('error', function (err) {
			console.error('Error!', err.message);
		})
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(paths.style.dist));
});


/**
 * Image optimisation
 */
gulp.task('images', function () {
	return gulp.src(paths.img.src+'**/*.*')
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}]
		}))
		.pipe(gulp.dest(paths.img.dist));
});


/**
 * Script processing
 */
gulp.task('lint', function() {
	return gulp.src([paths.js.src+'**/*.js', '!'+paths.js.src+'lib/**/*.js'])
		.pipe(jshint({'esnext':true}))
		.pipe(jshint.reporter('default'));
});
gulp.task('es2015', function(){
	return browserify({ entries: paths.js.src+'theme.js', debug: true })
		.transform(babelify)
		.bundle()
		.on('error', function(err){ console.log(err.stack); })
		.pipe(source('theme.js'))
		.pipe(gulp.dest(paths.js.dist));
});
gulp.task('strip', function(){
	return gulp.src(paths.js.dist+'theme.js')
		.pipe(stripDebug())
		.pipe(gulp.dest(paths.js.dist));
});
gulp.task('uglify', function(){
	return gulp.src(paths.js.dist+'theme.js')
		.pipe(uglify())
		.pipe(gulp.dest(paths.js.dist));
});
gulp.task('copyLibsToDist', function(){
	return gulp.src(paths.js.src+'lib/**/*.js')
		.pipe(gulp.dest(paths.js.dist+'lib'));
})
gulp.task('scripts', function(){
	if(is_dev) {
		runSequence(
			'lint',
			'es2015',
			'copyLibsToDist'
		);
	} else {
		runSequence(
			'lint',
			'es2015',
			'strip',
			'uglify',
			'copyLibsToDist'
		);
	}
});


/**
 * Watch tower
 */
gulp.task('watchtower', function(){

	// watch for image changes
	gulp.watch(paths.img.src+'**/*.*', ['images']);

	// watch for style changes
	gulp.watch(paths.style.src+'**/*.scss', ['sass']);

	// watch for script changes
	gulp.watch(paths.js.src+'**/*.js', ['scripts']);

});


/**
 * Build tasks
 */
function sequence(){
	runSequence(
		'housekeeping',
		'sass',
		'images',
		'scripts',
		'watchtower'
	);
}

// default "live"
gulp.task('default', function(){
	sequence();
});

// dev "debug"
gulp.task('dev', function(){
	is_dev = true;
	sequence();
});