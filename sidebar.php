<div class="sidebar"><?php

	if ( ! dynamic_sidebar( 'main-sidebar' ) ) : ?>

		<section>
			<h2 class="widget-title"><?php _e( 'Archives', 'namespace' ); ?></h2>
			<ul>
				<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
			</ul>
		</section>

		<section>
			<h2 class="widget-title"><?php _e( 'Meta', 'namespace' ); ?></h2>
			<ul>
				<?php wp_register(); ?>
				<li><?php wp_loginout(); ?></li>
				<?php wp_meta(); ?>
			</ul>
		</section> <?php

	endif; // end sidebar widget area ?>
</div>
