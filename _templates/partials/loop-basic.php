<?php

	if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<article <?php post_class(); ?>>

			<header class="post__header"><?php
				if(is_singular()): ?>
					<h1><?php the_title() ?></h1><?php
				else: ?>
					<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2><?php
				endif; ?>
			</header>

			<div class="post__content"><?php

				if(is_singular()):
					the_post_thumbnail('large');
					the_content();	// full article
				else:
					the_post_thumbnail('thumbnail');
					the_excerpt();	// article listing
				endif;?>

			</div>

		</article><?php

	endwhile; endif;

?>
