<?php

$prev_post = get_adjacent_post(false, '', true);
$next_post = get_adjacent_post(false, '', false);

?>

<div class="pagination">
	<div class="grid">
		<div class="prev-post grid__item one-whole lap-and-up-one-half"> <?php
			if(!empty($prev_post)) : ?>
				<span class="next-prev">Previous post:</span>
				<a href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php echo $prev_post->post_title ?>"><?php
					echo $prev_post->post_title; ?>
				</a><?php
			endif; ?>
		</div>

		<div class="next-post grid__item one-whole lap-and-up-one-half"><?php
			if(!empty($next_post)) : ?>
				<span class="next-prev">Next post:</span>
				<a href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo $next_post->post_title ?>"><?php
					echo $next_post->post_title; ?>
				</a><?php
			endif; ?>
		</div>
	</div>
</div>