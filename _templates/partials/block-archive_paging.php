<?php

global $wp_query;

function has_paging(){
	$prev_link = get_previous_posts_link();
	$next_link = get_next_posts_link();

	if($prev_link || $next_link){
		return true;
	} else {
		return false;
	}
}

?>

<?php if (has_paging()) : ?>
<div class="pagination"><?php
	$big = 999999999; // need an unlikely integer
	$translated = __( 'Page', 'mytextdomain' ); // Supply translatable string

	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>',
		'after_page_number' => ' | '
	) );
	?>
</div>
<?php endif; ?>