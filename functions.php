<?php

// store the theme URL
$template_url = get_template_directory_uri();
$template_dir = get_template_directory();


/**
 * Some helpful "constants"
 */
define( 'PATH_THEME', $template_url . '/' );                        //theme root URL
define( 'PATH_TEMPLATES', '_templates/' );                          //theme templates
define( 'PATH_PARTIALS', '_templates/partials/' );                  //theme template partials
define( 'PATH_IMG', $template_url . '/dist/img/' );                 //theme images dir
define( 'PATH_JS', $template_url . '/dist/js/' );                   //theme javascript dir
define( 'PATH_PHP', $template_dir . '/_functional/' );              //theme php dir
define( 'ACF', class_exists('acf') );                               //check if advanced custom fields is installed


/**
 * Includes
 */
include_once( PATH_PHP . 'wordpress-overrides.php' );   //overrides for core wordpress stuff
include_once( PATH_PHP . 'post-types.php' );            //sets up the custom post types for the theme
include_once( PATH_PHP . 'navigation.php' );            //registers navigation for the theme
include_once( PATH_PHP . 'shortcodes.php' );            //registers shortcodes
include_once( PATH_PHP . 'widgets.php' );               //registers widget areas and widgets
include_once( PATH_PHP . 'dev-utils.php' );             //useful developer functions
include_once( PATH_PHP . 'theme-setup.php' );           //stuff to setup on every project


/**
 * checks to see if an ACF field has a value assigned
 */
function checkFieldHasValue($fieldname, $option=false)
{
	if (!ACF) { return false; }

	$field = ($option) ? get_field($fieldname, 'option') : get_field($fieldname) ;
	if (!empty($field))
	{
		return true;
	}
}


/**
 * WOOCOMMERCE
 */

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

function jigowatt_theme_wrapper_start()
{
?>
<div class="grid">
	<div class="grid__item one-whole">
		<?php get_template_part( PATH_PARTIALS.'/page' , 'loop' ); ?>
<?php
}
add_action( 'woocommerce_before_main_content', 'jigowatt_theme_wrapper_start', 10 );


function jigowatt_theme_wrapper_end()
{
?>
	</div>
	<!--div class="grid__item one-whole lap-and-up-one-quarter"-->
		<!--?php get_sidebar(); ?-->
	<!--/div-->
</div>
<?php
}
add_action( 'woocommerce_after_main_content', 'jigowatt_theme_wrapper_end', 10 );


function jigowatt_woocommerce_support()
{
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'jigowatt_woocommerce_support' );


/**
 * Default loop columns on product archives
 * @return integer products per row
 */
function jigowatt_loop_columns() {
	return apply_filters( 'jigowatt_loop_columns', 3 ); /* 3 products per row */
}
add_filter( 'loop_shop_columns', 'jigowatt_loop_columns' );

