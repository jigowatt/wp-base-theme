<?php

/**
 * Jigowatt logging
 */
if ( ! function_exists( 'jigowatt_log' ))
{
	/**
	 * Logs to the debug log when you enable wordpress debug mode.
	 *
	 * @param string $from_file is the name of the php file that you are logging from.
	 * defaults to JIGOWATT if non is supplied.
	 * @param mixed $message this can be a regular string, array or object
	 */
	function jigowatt_log( $message, $from_file = 'JIGOWATT' )
	{
		if ( WP_DEBUG === true ) {
			if ( is_array( $message ) || is_object( $message )) {
				error_log( $from_file.': '.print_r($message, true) );
			} else {
				error_log( $from_file.': '.$message );
			}
		}
	}
}
jigowatt_log( '@@@@@  BEGINNING OF RUN IN JIGOWATT CUSTOM FUNCTIONS.PHP  @@@@@' );

?>