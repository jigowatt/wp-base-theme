<?php

/**
 * Register menus
 */
register_nav_menus( array(
	'utility_menu' => 'Utility menu (usually home to less visited but important links)',
	'main_menu' => 'Menu in the header of the site',
	'footer_menu' => 'Footer links'
) );

?>