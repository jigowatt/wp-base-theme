<?php
/**
 * Creates a new grid container
 * @param  string $content content to put between the div tags
 * @return string          html for the grid container
 */
function grid_func($atts, $content=null)
{
	return '<div class="grid">'.do_shortcode(shortcode_unautop($content)).'</div>';
}
add_shortcode( 'grid', 'grid_func' );


/**
 * Creates a new grid column
 * @param  array $atts    group of options
 * @param  string $content content to put between the div tags
 * @return string          html for the column
 */
function grid_item_func($atts, $content=null)
{
	extract( shortcode_atts( array(
		'palm' => '',
		'lap' => '',
		'lap_and_up' => '',
		'desk' => '',
		'desk_wide' => '',
		'default' => ''
	), $atts));

	$class = '';

	if(!empty($atts)){
		foreach ($atts as $name => $value) {

			if(!empty($value)){
				switch ($name){
					case 'palm':
						$class .= 'palm-'.$value.' ';
						break;
					case 'lap':
						$class .= 'lap-'.$value.' ';
						break;
					case 'lap_and_up':
						$class .= 'lap-and-up-'.$value.' ';
						break;
					case 'desk':
						$class .= 'desk-'.$value.' ';
						break;
					case 'desk_wide':
						$class .= 'desk-wide-'.$value.' ';
						break;
					case 'default':
						$class .= $value.' ';
						break;
				}
			}
		}
	}

	return '<div class="grid__item '.$class.'">'.do_shortcode(shortcode_unautop($content)).'</div>';
}
add_shortcode( 'grid_item', 'grid_item_func' );