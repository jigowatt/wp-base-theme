<?php

/**
 * Add Theme Support
 */
add_action( 'after_setup_theme', 'jigowatt_theme_supports' );
function jigowatt_theme_supports()
{
	add_theme_support( 'post-thumbnails' ); //thumbnails (featured image)
	add_theme_support( 'menus' );           //custom menus
	add_theme_support( 'html5' );           //html5
}


/**
 * Load Scripts and Styles
 *
 * Note: IE specific imports can found further down this file
 */
add_action( 'wp_enqueue_scripts', 'jigowatt_enqueue_all_the_things' );
function jigowatt_enqueue_all_the_things()
{

	// Scripts
	wp_enqueue_script( 'theme', PATH_JS.'theme.js', array( 'jquery' ) );

	// enable some PHP values to be available to our JavaScript
	$phpVars = array(
		'js' => PATH_JS,
		'img' => PATH_IMG,
		'uploads' => site_url()."/wp-content/uploads/"
	);
	wp_localize_script( 'theme', 'phpVars', $phpVars );

	// Styles
	wp_register_style( 'main', PATH_THEME.'style.css' );
	wp_enqueue_style( 'main' );
}


/**
 * Add page/post slug to <body> "class" attribute
 */
add_filter( 'body_class', 'jigowatt_add_body_class' );
function jigowatt_add_body_class( $classes )
{
	global $post;

	if ( isset( $post ) ) {
		$classes[] = $post->post_type . ' ' . $post->post_name;
	}

	$blogname = get_bloginfo( 'name' );

	$classes[] = sanitize_title( $blogname );

	return $classes;
}


/**
 * Prevent WordPress being a douchebag with <p> and <br> and shortcodes
 */
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop', 99 );
add_filter( 'the_content', 'shortcode_unautop', 100 );


/*  IE js header
/* ------------------------------------ */
function jigowatt_ie_js_header () {
	echo '<!--[if lt IE 9]>'. "\n";
	echo '<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>'. "\n";
	echo '<script src="' . PATH_JS . 'lib/selectivizr.min.js"></script>'. "\n";
	echo '<![endif]-->'. "\n";
}
add_action( 'wp_head', 'jigowatt_ie_js_header' );

/*  IE js footer
/* ------------------------------------ */
function jigowatt_ie_js_footer () {
	echo '<!--[if lt IE 9]>'. "\n";
	echo '<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>'. "\n";
	echo '<![endif]-->'. "\n";
}
add_action( 'wp_footer', 'jigowatt_ie_js_footer', 20 );


/**
 * Add Advanced Custom Fields Options page when plugin is active
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


/**
 * Prevent DDoS attacks using pingbacks
 */
function remove_x_pingback($headers) {
	unset($headers['X-Pingback']);
	return $headers;
}
add_filter('wp_headers', 'remove_x_pingback');

?>