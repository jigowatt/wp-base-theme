<?php

/**
 * Register widget areas
 */
register_sidebar( array(
	'name'          => __( 'Main sidebar', 'namespace' ),
	'id'            => 'main-sidebar',
	'description'   => '',
	'class'         => '',
	'before_widget' => '<section id="%1$s" class="widget %2$s">',
	'after_widget'  => '</section>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );


?>