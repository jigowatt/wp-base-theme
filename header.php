<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0" />

	<?php //@TODO: install yoast SEO or uncomment below metas ?>

	<!-- opengraph (facebook) -->
	<!-- <meta property="og:type" content="website" />
	<meta property="og:site_name" content="<?php bloginfo('name') ?>" />
	<meta property="article:author" content="https://www.facebook.com/PAGE_NAME_HERE" />
	<meta property="og:url" content="<?php bloginfo('url') ?>" />
	<meta property="og:title" content="<?php bloginfo('name') ?>" />
	<meta property="og:description" content="<?php bloginfo('description') ?>" /> -->

	<!-- Twitter -->
	<!-- <meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@" />
	<meta name="creator" content="@kersley" />
	<meta name="twitter:domain" content="<?php bloginfo('url') ?>" /> -->

	<!-- windows 8 -->
	<!-- <meta name="msapplication-TileColor" content="#efefef" />
	<meta name="msapplication-TileImage" content="<?php echo PATH_IMG ?>logo.png" />
	<meta name="application-name" content="<?php bloginfo('name') ?>" /> -->

	<!-- page title -->
	<title><?php wp_title() ?></title>

	<!-- iOS -->
	<link rel="apple-touch-icon-precomposed" href="<?php echo PATH_IMG ?>apple-touch-icon-precomposed.png">

	<!-- favicon -->
	<link rel="shortcut icon" href="<?php echo PATH_IMG ?>favicon.ico" type="image/x-icon" />

	<?php wp_head(); ?>


</head>

<body <?php body_class('no-js') ?>>

	<header class="main-header">

		<div class="contain">

			<div class="grid">

				<div class="grid__item one-whole">
					<nav class="utility">
						<?php
							$defaults = array(
								'theme_location'  => 'utility_menu',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'menu menu--utility',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 1,
								'walker'          => ''
							);

							wp_nav_menu( $defaults );
						?>
					</nav>

					<?php get_search_form(); ?>
				</div>

				<div class="grid__item one-whole">
					<div class="branding">
						<?php //@TODO: add branding ?>
					</div>
				</div>

				<div class="grid__item one-whole">
					<nav class="main">
						<?php
							$defaults = array(
								'theme_location'  => 'main_menu',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'menu menu--main',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
								'walker'          => ''
							);

							wp_nav_menu( $defaults );
						?>
					</nav>
				</div>

			</div>

		</div>

	</header>

	<div class="main-content">
		<div class="contain">