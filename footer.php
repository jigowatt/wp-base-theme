		</div><!-- .contain -->
	</div><!-- .content-container -->

	<footer class="main-footer">

		<div class="contain">

			<div class="grid">
				<div class="grid__item one-whole">
					<nav class="menu--footer">
						<?php
							$defaults = array(
								'theme_location'  => 'footer_menu',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'menu menu--footer',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => '',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
								'walker'          => ''
							);

							wp_nav_menu( $defaults );
						?>
					</nav>

				</div>
			</div>
		</div>

	</footer>

	<?php wp_footer() ?>

</body>
</html>