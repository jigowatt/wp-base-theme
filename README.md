# Jigowatt Client Theme (please read)
This theme is developed using the "Jigowatt Client Theme" framework.
You should adhere to our [Development Practices](#markdown-header-development-practices) if you're using this theme.

## Getting Started (brand new project)

> **NOTE: REMOVE THIS SECTION OF THE DOCUMENT WHEN YOU'VE CREATED THE PROJECT**

To use this theme you'll need to copy it into `/wp-content/wp-themes/`.

You can do this through terminal by following these commands:

```bash
$ # CD into the wp-content/themes/ directory
$ cd YOURPROJECT/wp-content/themes/

$ # Grab the repository for the theme and put it in the themes directory
$ git clone --recursive https://mattkersley@bitbucket.org/jigowatt/wp-base-theme.git ./THEMENAME

$ # CD into your new theme directory
$ cd THEMENAME

$ # Detatch the remote repository so we don't modify the original repo
$ git remote rm origin
```

> **Note:**
> The .gitignore files and history will be preserved, but you will no longer be able to push the repository to a remote server.
> If you wish for this theme folder to have it's own remote repository, it'll need to be added manually.

## Installing GulpJS and It's Plugins
The theme makes use of GulpJS for processing JavaScript, Sass and Optimising images.

> **Important:**
> GulpJS is a NodeJS package. Because of this you'll need to install Node on your machine.
> You can [download the .pkg installer](http://nodejs.org/) from their site and install it.

To install GulpJS and it's dependencies, you'll need to run the following in terminal:

```bash
$ # Install the Sass Ruby Gem
$ sudo gem install sass

$ # Install gulp and it's plugins globally for the machine
$ sudo npm install -g babelify browserify gulp gulp-imagemin gulp-jshint gulp-ruby-sass gulp-strip-debug gulp-uglify gulp-watch run-sequence vinyl-source-stream

$ # Go to your theme directory
$ cd [theme-directory-path]

$ # Should install the node modules related to the project
$ sudo npm install
$ # seems this is needed to get the local version of gulp upgraded to match global version
$ sudo npm install gulp --save-dev
```

## Running Gulp and Getting to Work
Now you can start GulpJS watching your theme directory for any changes, and get to work.

There are two gulp commands you'll need, one for development, and one for producing the minified JS and CSS:


```bash
$ # Use this one while you need to debug stuff
$ gulp dev

$ # Use this one for producing production ready files
$ gulp
```

## Development Practices

### !! Important files !!
The theme framework relies on the following files to remain intact, and changed only if you know what you're doing.

```bash
.sass-cache/          # created by gulp, don't remove
./node_modules/       # where gulp lives
.gitignore            # tells git what to exclude when saving changes
.gitmodules           # used to keep InuitCSS up-to-date
gulpfile.js           # tells gulp what to do
package.json          # keeps a list of gulp dependencies
style.css             # DONT EDIT: this is the result of the sass processing
```

### Source files (images, js, sass)
Images, JavaScript and Styling should only be saved/modified in the `./src/` directory.
Gulp processes the `./src/` directory and outputs to `./dist/` for use in the live website.

### Partials (template blocks)
The `./_templates/partials/` directory is to be used for "blocks" of re-usable template code.
Anything that appears more than once in the site should be extracted out into a separate partial file.

To include the partials in your theme you should use:
`get_template_part(PATH_PARTIALS.'[block-group]-[block-name]');`

Here's an example of it's usage with meaningful block groups and names:
```php
get_template_part(PATH_PARTIALS.'post-meta');  # post date, category and comment count
get_template_part(PATH_PARTIALS.'post-share'); # social sharing links
get_template_part(PATH_PARTIALS.'author-bio'); # an author's biography
```

The above code would pull in the following files:
```bash
._templates/partials/post-meta.php
._templates/partials/post-share.php
._templates/partials/author-bio.php
```
