<?php get_header(); ?>

<?php

	get_template_part(PATH_PARTIALS.'loop-basic');

	// If we're on a single post or page
	if(is_single()){

		// output next/prev paging
		get_template_part(PATH_PARTIALS.'block-single_paging');

		// and comments are enabled
		if ( comments_open() || get_comments_number() ) :

			// output comments and comments form
			comments_template();
		endif;

		// output next/prev paging
		get_template_part(PATH_PARTIALS.'block-single_paging');
	}

	// If we're on a "listing" page (archive)
	else {

		// get numbered paging
		get_template_part(PATH_PARTIALS.'block-archive_paging');

	}



?>

<?php get_footer(); ?>